﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopTryCatch.Contexts;
using ShopTryCatch.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopTryCatch.Tests
{
    [TestClass]
    public class ShopTryCatchTest
    {
        // Making the services available in inheriting test clases.
        internal IProductService productService = ShopTryCatch.Services.ServiceProvider.ProductsService();
        internal IOrderService orderService = ShopTryCatch.Services.ServiceProvider.OrdersService();

        [AssemblyInitialize]
        public static void TestSetup(TestContext testContext)
        {
            // Reseting the database on every test batch.
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ""));
            using (var context = new ShopTryCatchContext())
                context.ReCreateDb();
        }
    }
}
