﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopTryCatch.Contexts;
using ShopTryCatch.Models;
using ShopTryCatch.Models.ViewModels;
using ShopTryCatch.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Validation;

namespace ShopTryCatch.Tests.Services
{
    [TestClass]
    public class OrderServiceTests : ShopTryCatchTest
    {
        [TestMethod]
        public async Task can_create_order()
        {
            var viewModel = new OrderCreation();
            viewModel.Address = "Test address";
            viewModel.City = "Test city";
            viewModel.Email = "perfectlyvalid@emailaddress.com";
            viewModel.FirstName = "Test person";
            viewModel.HouseNumber = "24Test";
            viewModel.LastName = "Test last name";
            viewModel.Products = new List<OrderProductCreation>() {
                new OrderProductCreation { Amount = 2, ProductId = 1},
                new OrderProductCreation { Amount = 5, ProductId = 2}
            };
            viewModel.ZipCode = 12587;
            int id = await orderService.Create(viewModel);
            Assert.IsTrue(id > 0, "The entity was reported as not created");
            using (var context = new ShopTryCatchContext())
            {
                Order order = context.Orders.Include(o => o.OrderProducts).SingleOrDefault(o => o.ID == id);
                Assert.IsNotNull(order, "The entity wasn't actually created");
                Assert.IsTrue(order.OrderProducts.Count == 2, "An incorrect amount of products were added.");
            }
        }

        [TestMethod]
        public async Task cant_create_order_with_invalid_email()
        {
            var viewModel = new OrderCreation();
            viewModel.Address = "Test address";
            viewModel.City = "Test city";
            viewModel.Email = "notanemail";
            viewModel.FirstName = "Test person";
            viewModel.HouseNumber = "24Test";
            viewModel.LastName = "Test last name";
            viewModel.Products = new List<OrderProductCreation>() {
                new OrderProductCreation { Amount = 2, ProductId = 1},
                new OrderProductCreation { Amount = 5, ProductId = 2}
            };
            viewModel.ZipCode = 12587;
            try
            {
                int id = await orderService.Create(viewModel);
                Assert.Fail("You are not supposed to be able to create an order without a valid email address");
            }
            catch (DbEntityValidationException) { /* Failing as expected */ }
        }
    }
}
