﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopTryCatch.Models;
using ShopTryCatch.Models.ViewModels;
using ShopTryCatch.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ShopTryCatch.Tests.Services
{
    [TestClass]
    public class ProductServiceTests : ShopTryCatchTest
    {
        [TestMethod]
        public void can_obtain_first_page_of_products()
        {
            ProductPage firstPage = productService.GetPage(0);
            Assert.IsTrue(firstPage.Elements.Count == 10, "Didn't got the expected amount of products.");
            Assert.IsTrue(firstPage.Elements.First().ID == 1, "Didn't got the first page.");
        }

        [TestMethod]
        public void can_obtain_second_page_of_products()
        {
            ProductPage firstPage = productService.GetPage(1);
            Assert.IsTrue(firstPage.Elements.First().ID == 11, "Didn't got the second page.");
        }

        [TestMethod]
        public void can_obtain_product_list_from_ids()
        {
            List<Product> products = productService.GetCollection(new int[] { 1, 6, 8 });
            Assert.IsTrue(products.Count == 3, "Didn't got the correct size collection.");
            Assert.IsTrue(products.Any(p => p.ID == 1), "Didn't got a desired item in the collection.");
            Assert.IsTrue(products.Any(p => p.ID == 6), "Didn't got a desired item in the collection.");
            Assert.IsTrue(products.Any(p => p.ID == 8), "Didn't got a desired item in the collection.");
        }
    }
}
