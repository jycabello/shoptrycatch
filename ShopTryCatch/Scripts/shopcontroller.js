﻿var shopApp = angular.module('shopApp', []);

shopApp.controller('ShopCtrl', function ($scope, $http) {
    // Loading of the first page.
    $scope.page = JSON.parse(angular.element("#first-page").val());

    // Retrieving possible cart from sessionStorage.
    $scope.order = {};
    if (!sessionStorage.cart) {
        $scope.cart = [];
    } else {
        $scope.cart = JSON.parse(sessionStorage.cart);
        setOrderProducts();
    }

    // Reset after a successful creation.
    function orderCreated() {
        $scope.order = {};
        $scope.cart = [];
        saveCart();
        $scope.changeScreen($scope.thankYouScreen);
        $scope.orderForm.$setPristine();
    }

    // Pages, being just four and one behind the other, I decided not to use routes.
    $scope.productListScreen = 0;
    $scope.reviewScreen = 1;
    $scope.orderScreen = 2;
    $scope.thankYouScreen = 3;
    $scope.currentScreen = 0;

    $scope.currentScreenIs = function (screen) {
        return $scope.currentScreen == screen;
    };

    $scope.changeScreen = function (screen) {
        $scope.closeModals();
        $scope.currentScreen = screen;
    };

    // Cart basic functionality.
    $scope.cartCount = function () {
        return _.sum($scope.cart, function (element) {
            return element.Amount;
        });
    };

    $scope.addToCart = function (product) {
        if (_.any($scope.cart, { ID: product.ID })) {
            _.forEach($scope.cart, function (element) {
                element.Amount++;
            });
        } else {
            product.Amount = 1;
            $scope.cart.push(product);
        }
        saveCart();
    };

    $scope.removeFromCart = function (id) {
        _.remove($scope.cart, function (element) {
            return element.ID == id;
        });
        saveCart();
    };

    function saveCart() {
        sessionStorage.cart = JSON.stringify($scope.cart);
        setOrderProducts();
    }
    function setOrderProducts() {
        $scope.order.Products = _.map($scope.cart, function (element) {
            return { ProductId: element.ID, Amount: element.Amount };
        });
    }

    // Page loader.
    $scope.getPage = function (pageNumber) {
        $http({
            url: "/api/Products",
            method: "GET",
            params: { page: pageNumber }
        }).then(function (response) {
            $scope.page = response.data;
        }, null); // We are not contemplating errors since is not within the scope to make a complete application.
    };

    // Cart basic functionality.
    $scope.openCart = function () {
        var modal = $("#cart-modal");
        modal.modal("show");
    };

    $scope.cartTotalUntaxed = function () {
        return _.sum($scope.cart, function (element) {
            return element.Price * element.Amount;
        });
    };

    $scope.cartTaxes = function () {
        return _.sum($scope.cart, function (element) {
            return element.Tax * element.Amount;
        });
    };

    $scope.cartTotal = function () {
        return _.sum($scope.cart, function (element) {
            return element.FinalPrice * element.Amount;
        });
    };

    $scope.productDetails = function (product) {
        $scope.selectedProduct = product;
        var modal = $("#details-modal");
        modal.modal("show");
    };

    $scope.closeModal = function (id) {
        $(id).modal("hide");
    };

    $scope.closeModals = function () {
        $(".modal").modal("hide");
    };

    // Order submitting.
    $scope.submitOrder = function () {
        $http({
            url: "/api/Orders",
            method: "POST",
            data: $scope.order
        }).then(function (response) {
            orderCreated();
        }, null);
    };
});