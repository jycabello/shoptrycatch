﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace ShopTryCatch.Models
{
    /// <summary>
    /// Products sold in the store.
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Product identifier.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Product name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Product description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Product price, untaxed.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Taxes to apply.
        /// </summary>
        public decimal Tax
        {
            get
            {
                return Price * (decimal.Parse(ConfigurationManager.AppSettings["VATPercentage"]) / 100);
            }
        }

        /// <summary>
        /// Taxed price.
        /// </summary>
        public decimal FinalPrice
        {
            get
            {
                return Price + Tax;
            }
        }

        /// <summary>
        /// The amount of product, only used in the cart.
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// Factory from xml element.
        /// </summary>
        /// <param name="xElement">The xml element.</param>
        /// <returns>Object build from xml element.</returns>
        internal static Product FromXElement(XElement xElement)
        {
            return new Product()
            {
                Description = xElement.Element("description").Value,
                ID = int.Parse(xElement.Element("id").Value),
                Name = xElement.Element("name").Value,
                Price = decimal.Parse(xElement.Element("price").Value)
            };
        }
    }
}