﻿using ShopTryCatch.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ShopTryCatch.Models
{
    /// <summary>
    /// Relation between orders and products.
    /// </summary>
    public class OrderProduct
    {
        /// <summary>
        /// Entity identifier.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Order identifier.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Order reference.
        /// </summary>
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }

        /// <summary>
        /// Product identifier.
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Product, not mapped, an eager loading is required.
        /// </summary>
        [NotMapped]
        public Product Product { get; set; }

        /// <summary>
        /// Amount of product.
        /// </summary>
        // We could store the price at the buy time too so we can change prices
        // without affecting emmited orders. However, it goes beyond the scope
        // of the excercise.
        public int Amount { get; set; }

        /// <summary>
        /// Factory from ViewModel.
        /// </summary>
        /// <param name="viewModel">ViewModel</param>
        /// <param name="order">Order.</param>
        /// <returns>The model instance.</returns>
        public static OrderProduct FromViewModel(OrderProductCreation viewModel, Order order)
        {
            return new OrderProduct()
            {
                Amount = viewModel.Amount,
                Order = order,
                ProductId = viewModel.ProductId
            };
        }
    }
}