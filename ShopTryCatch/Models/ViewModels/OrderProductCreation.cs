﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopTryCatch.Models.ViewModels
{
    /// <summary>
    /// ViewModel to relate orders with the ordered products.
    /// </summary>
    public class OrderProductCreation
    {
        /// <summary>
        /// Product identifier.
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Amount of product.
        /// </summary>
        public int Amount { get; set; }
    }
}