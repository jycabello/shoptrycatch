﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopTryCatch.Models.ViewModels
{
    public class ProductPage
    {
        /// <summary>
        /// Current page number.
        /// </summary>
        public int PageNum { get; set; }

        /// <summary>
        /// Max page number.
        /// </summary>
        public int MaxPage
        {
            get
            {
                int result = TotalElementCount / ElementsPerPage;
                if (TotalElementCount % ElementsPerPage > 0)
                    result++;
                return result;
            }
        }

        /// <summary>
        /// Number of elements.
        /// </summary>
        public int TotalElementCount { get; set; }

        /// <summary>
        /// Max amount of elements contained in a page.
        /// </summary>
        public int ElementsPerPage
        {
            get
            {
                // As requested in requisites.
                return 10;
            }
        }

        /// <summary>
        /// Elements on the page.
        /// </summary>
        public List<Product> Elements { get; set; }
    }
}