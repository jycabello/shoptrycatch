﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopTryCatch.Models.ViewModels
{
    /// <summary>
    /// ViewModel to create an order.
    /// </summary>
    public class OrderCreation
    {
        /// <summary>
        /// Client's first name.
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Client's last name.
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Client's address.
        /// </summary>
        [Required]
        public string Address { get; set; }

        /// <summary>
        /// Client's house number.
        /// </summary>
        [Required]
        public string HouseNumber { get; set; }

        /// <summary>
        /// Client's area zip code.
        /// </summary>
        [Required]
        public int ZipCode { get; set; }

        /// <summary>
        /// Client's residence city.
        /// </summary>
        [Required]
        public string City { get; set; }

        /// <summary>
        /// Client's email.
        /// </summary>
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        /// <summary>
        /// Ordered products, key as ID and value as amount.
        /// </summary>
        public List<OrderProductCreation> Products { get; set; }

        /// <summary>
        /// Creates the model.
        /// </summary>
        public Order Model
        {
            get
            {
                var result = new Order()
                {
                    Address = Address,
                    City = City,
                    Email = Email,
                    FirstName = FirstName,
                    HouseNumber = HouseNumber,
                    LastName = LastName,
                    OrderedAt = DateTime.Now,
                    ZipCode = ZipCode
                };
                result.OrderProducts = Products.Select(p => OrderProduct.FromViewModel(p, result)).ToList();
                return result;
            }
        }
    }
}