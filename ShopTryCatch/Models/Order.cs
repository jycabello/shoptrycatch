﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopTryCatch.Models
{
    /// <summary>
    /// Client order.
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Entity identifier.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Client's first name.
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Client's last name.
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Client's address.
        /// </summary>
        [Required]
        public string Address { get; set; }

        /// <summary>
        /// Client's house number.
        /// </summary>
        [Required]
        public string HouseNumber { get; set; }

        /// <summary>
        /// Client's area zip code.
        /// </summary>
        [Required]
        public int ZipCode { get; set; }

        /// <summary>
        /// Client's residence city.
        /// </summary>
        [Required]
        public string City { get; set; }

        /// <summary>
        /// Client's email.
        /// </summary>
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Date of order.
        /// </summary>
        public DateTime OrderedAt { get; set; }

        /// <summary>
        /// Relation with ordered products.
        /// </summary>
        public virtual List<OrderProduct> OrderProducts { get; set; }
    }
}