﻿using ShopTryCatch.Models;
using ShopTryCatch.ServiceInterfaces;
using ShopTryCatch.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopTryCatch.Services
{
    /// <summary>
    /// Service provider, injects dependencies when required.
    /// </summary>
    public static class ServiceProvider
    {
        /// <summary>
        /// Factory for product service.
        /// </summary>
        /// <returns>The required service.</returns>
        public static IProductService ProductsService()
        {
            return new ProductService();
        }

        /// <summary>
        /// Factory for orders service, injects the default products service.
        /// </summary>
        /// <returns>The required service.</returns>
        public static IOrderService OrdersService()
        {
            return new OrderService(ProductsService());
        }
    }
}