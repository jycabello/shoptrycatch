﻿using ShopTryCatch.Models;
using ShopTryCatch.Models.ViewModels;
using ShopTryCatch.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace ShopTryCatch.Services
{
    public class ProductService : IProductService
    {
        /// <summary>
        /// Path for the xml file.
        /// </summary>
        private string xmlPath
        {
            get
            {
                // For testability, if the context is null, it will go for the test path.
                if (HttpContext.Current != null)
                    return HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["ProductXMLPath"]);
                else
                    return System.Configuration.ConfigurationManager.AppSettings["ProductXMLPath"];
            }
        }

        /// <summary>
        /// XML document.
        /// </summary>
        private XDocument xDocument
        {
            get
            {
                return XDocument.Load(xmlPath);
            }
        }

        private List<Product> _products = null;
        /// <summary>
        /// Products in the xml file.
        /// </summary>
        private List<Product> Products
        {
            get
            {
                // If we were to have a real big product list, we would have to read the XML line by line
                // and parse it "on the go", or switch to SQL. Since we're not, I just load the list on memory.
                if (_products == null)
                    _products = xDocument
                        .Element("products")
                        .Descendants("product")
                        .Select(xe => Product.FromXElement(xe))
                        .OrderBy(p => p.ID)
                        .ToList();
                return _products;
            }
        }

        /// <summary>
        /// Gets a paged list of products
        /// </summary>
        /// <param name="page">Page number</param>
        /// <returns>A page of the product list</returns>
        public ProductPage GetPage(int page = 0)
        {
            var result = new ProductPage();
            result.PageNum = page;
            result.TotalElementCount = Products.Count;
            result.Elements = Products.Skip(page * result.ElementsPerPage).Take(result.ElementsPerPage).ToList();
            return result;
        }

        /// <summary>
        /// Gets a product collection based on the ids.
        /// </summary>
        /// <param name="ids">Ids of the wanted products.</param>
        /// <returns>A product collection.</returns>
        public List<Product> GetCollection(int[] ids)
        {
            return Products.Where(p => ids.Contains(p.ID)).ToList();
        }
    }
}