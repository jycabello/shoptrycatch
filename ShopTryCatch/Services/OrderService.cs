﻿using ShopTryCatch.Contexts;
using ShopTryCatch.Models;
using ShopTryCatch.Models.ViewModels;
using ShopTryCatch.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ShopTryCatch.Services
{
    public class OrderService : IOrderService
    {
        /// <summary>
        /// Product service, required to check if the ordered products exist.
        /// </summary>
        private IProductService productService;

        /// <summary>
        /// Default constructor, requires a product service to be injected.
        /// </summary>
        /// <param name="productService">A instance of product service.</param>
        public OrderService(IProductService productService)
        {
            this.productService = productService;
        }

        /// <summary>
        /// Creates a new order.
        /// </summary>
        /// <param name="viewModel">ViewModel to create the entity.</param>
        /// <returns>The id of the new order.</returns>
        public async Task<int> Create(OrderCreation viewModel)
        {
            // First of all, we need to check if we are trying to add non existing products.
            var products = productService.GetCollection(viewModel.Products.Select(p => p.ProductId).ToArray());
            if (products.Count != viewModel.Products.Count)
                return 0; // We should actually throw an excepcion or return a more complex object to inform the result
                          // but for the sake of simplicity, a zero will be a "not created" result.
            var model = viewModel.Model;
            using (var context = new ShopTryCatchContext())
            {
                context.Orders.Add(model);
                await context.SaveChangesAsync();
            }
            return model.ID;
        }
    }
}