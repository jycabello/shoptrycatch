﻿using ShopTryCatch.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ShopTryCatch.Contexts
{
    public class ShopTryCatchContext : DbContext
    {
        /// <summary>
        /// Recreates de database.
        /// </summary>
        public void ReCreateDb() // For testing purposes, mostly.
        {
            if (this.Database.Exists())
                this.Database.Delete();
            this.Database.Create();
        }

        /// <summary>
        /// Product orders.
        /// </summary>
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Relation between the orders themselves and the products ordered in them.
        /// </summary>
        public DbSet<OrderProduct> OrderProducts { get; set; }
    }
}