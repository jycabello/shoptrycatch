﻿using ShopTryCatch.Models;
using ShopTryCatch.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopTryCatch.ServiceInterfaces
{
    // Products service, the get method is not actually used
    public interface IProductService
    {
        /// <summary>
        /// Gets a products page.
        /// </summary>
        /// <param name="page">The required page.</param>
        /// <returns>The paginated result.</returns>
        ProductPage GetPage(int page = 0);

        /// <summary>
        /// Gets a collection of products identified by their ids.
        /// </summary>
        /// <param name="ids">The ids of the desired products.</param>
        /// <returns>The requested collection.</returns>
        List<Product> GetCollection(int[] ids);
    }
}