﻿using ShopTryCatch.Models;
using ShopTryCatch.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ShopTryCatch.ServiceInterfaces
{
    /// <summary>
    /// Order service interface, only includes the methods needed for the excercise.
    /// </summary>
    public interface IOrderService
    {
        /// <summary>
        /// Creates a new order from a view model.
        /// </summary>
        /// <param name="viewModel">The creation view model.</param>
        /// <returns>The id of the created item.</returns>
        Task<int> Create(OrderCreation viewModel);
    }
}