﻿using ShopTryCatch.ServiceInterfaces;
using ShopTryCatch.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopTryCatch.Controllers
{
    public class HomeController : Controller
    {
        private IProductService _productsService = null;
        internal IProductService ProductsService
        {
            get
            {
                if (_productsService == null)
                    _productsService = ServiceProvider.ProductsService();
                return _productsService;
            }
        }

        //
        // GET: /
        public ActionResult Index()
        {
            // Pre-loading of the first page of products.
            return View(ProductsService.GetPage(0));
        }
    }
}