﻿using ShopTryCatch.Models.ViewModels;
using ShopTryCatch.ServiceInterfaces;
using ShopTryCatch.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ShopTryCatch.Controllers
{
    // Only the used API actions are implemented.
    public class OrdersController : ApiController
    {
        private IOrderService _ordersService = null;
        internal IOrderService OrdersService
        {
            get
            {
                if (_ordersService == null)
                    _ordersService = ServiceProvider.OrdersService();
                return _ordersService;
            }
        }

        public async Task Post(OrderCreation viewModel)
        {
            await OrdersService.Create(viewModel);
        }
    }
}