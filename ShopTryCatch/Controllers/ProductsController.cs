﻿using ShopTryCatch.Models.ViewModels;
using ShopTryCatch.ServiceInterfaces;
using ShopTryCatch.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ShopTryCatch.Controllers
{
    // Only the used API actions are implemented.
    public class ProductsController : ApiController
    {
        private IProductService _productsService = null;
        internal IProductService ProductsService
        {
            get
            {
                if (_productsService == null)
                    _productsService = ServiceProvider.ProductsService();
                return _productsService;
            }
        }

        public ProductPage Get(int? page)
        {
            page = page ?? 0;
            return ProductsService.GetPage(page.Value);
        }
    }
}