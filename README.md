# README #

Coding excercise for Try Catch.

* Simple web shop
* Allows to list products and place orders that get stored in a database
* Used angular to give a single page application experience and proccess all the data via api calls.
* Async calls on database access to reduce thread lock time.

### How do I get set up? ###

* It just requires to download nuget dependencies.